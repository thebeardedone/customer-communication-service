package utils

import (
	"database/sql"
	"errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/customer-communication-service/config"
	"strconv"
	"strings"
	"time"
)

// ContactRequest model contains:
//	* Id: an instances unique database identifier
//	* Name: the name of the person who sent the contact request
//	* Email: the email address of the contact
//	* Subject: the subject or title of the message
//	* Message: the message itself
//	* Unread: specifies whether the message has been marked as read
//	* CreatedDate: when the contact request was created
//	* LastUpdated: when the contact request was last modified
type ContactRequest struct {
	Id          int64     `json:"id,omitempty"`
	Name        string    `json:"name,omitempty"`
	Email       string    `json:"email,omitempty"`
	Subject     string    `json:"subject,omitempty"`
	Message     string    `json:"message,omitempty"`
	Unread      bool      `json:"unread,omitempty"`
	CreatedDate time.Time `json:"createdDate,omitempty"`
	LastUpdated time.Time `json:"lastUpdated,omitempty"`
}

// Returns a string indicating that a specific field can have a specified maximum length
func getMaxLengthErrorString(field string, maxLength int) string {
	return field + " can have at most " + strconv.Itoa(maxLength) + " characters"
}

// Validates a ContactRequest. An error will be returned if:
//	* name is empty
//	* name is longer than the value specified in MaxNameLength
//	* name contains a new line character
//	* email address fails validation (see ValidateEmailAddress)
//	* subject is empty
//	* subject is longer than the value specified in MaxSubjectLength
//	* subject contains a new line character
//	* message is empty
//	* message is longer than the value specified in MaxMessageLength
func (contactRequest *ContactRequest) Validate(context *config.Context) error {

	if len(contactRequest.Name) == 0 {
		return errors.New("name is mandatory")
	}

	if len(contactRequest.Name) > context.Config.MaxNameLength {
		return errors.New(getMaxLengthErrorString("name", context.Config.MaxNameLength))
	}

	if strings.Contains(contactRequest.Name, "\n") || strings.Contains(contactRequest.Name, "\r") {
		return errors.New("name contains an invalid character")
	}

	emailAddressValidationError := ValidateEmailAddress(contactRequest.Email)
	if emailAddressValidationError != nil {
		return emailAddressValidationError
	}

	if len(contactRequest.Subject) == 0 {
		return errors.New("subject is mandatory")
	}

	if len(contactRequest.Subject) > context.Config.MaxSubjectLength {
		return errors.New(getMaxLengthErrorString("subject", context.Config.MaxSubjectLength))
	}

	if strings.Contains(contactRequest.Subject, "\n") || strings.Contains(contactRequest.Subject, "\r") {
		return errors.New("subject contains an invalid character")
	}

	if len(contactRequest.Message) == 0 {
		return errors.New("message is mandatory")
	}

	if len(contactRequest.Message) > context.Config.MaxMessageLength {
		return errors.New(getMaxLengthErrorString("message", context.Config.MaxMessageLength))
	}

	return nil
}

// Creates a ContactRequest record in the database. An error is returned if the insert fails.
func (contactRequest *ContactRequest) Create(db *sql.DB) error {
	currentTime := time.Now().Truncate(time.Millisecond)
	contactRequest.CreatedDate = currentTime
	contactRequest.LastUpdated = currentTime

	_, insertionError := db.Exec(
		`INSERT INTO contact_requests (
					name, email, subject, message, created_date, last_updated
				) VALUES ($1, $2, $3, $4, $5, $6)`,
				contactRequest.Name, contactRequest.Email, contactRequest.Subject, contactRequest.Message,
				contactRequest.CreatedDate, contactRequest.LastUpdated,
		)

	if insertionError != nil {
		logrus.WithFields(logrus.Fields{
			"contactRequest": contactRequest,
			"error": insertionError,
		}).Error("failed to persist contact request")
		return errors.New("failed to create contact request")
	}

	return nil
}