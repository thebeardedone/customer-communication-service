package utils

import (
	"errors"
	"regexp"
	"strings"
)

const invalidEmail = "invalid email address"

// Email address validation method. Nil is returned if the email address is valid.
//
// The following checks are performed and an error is returned if one of the following conditions is met:
//	* email address has a length of 0
//	* email address has a length greater than 254 characters
//	* invalid email address format
//	* identifier before the @ symbol must have a maximum length of 64 characters
func ValidateEmailAddress(email string) error {

	if len(email) == 0 {
		return errors.New("email address is mandatory")
	}

	if len(email) > 254 {
		return errors.New(invalidEmail)
	}

	match, regexError := regexp.MatchString("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$)", email)

	if regexError != nil {
		return errors.New("failed to parse email address")
	}

	if !match {
		return errors.New(invalidEmail)
	}

	atSymbolIndex := strings.Index(email, "@")

	if atSymbolIndex > 64 || len(email)-atSymbolIndex+1 > 255 {
		return errors.New(invalidEmail)
	}

	return nil
}
