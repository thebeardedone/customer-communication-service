package utils

import (
	"database/sql"
	"errors"
	"github.com/sirupsen/logrus"
	"time"
)

// Subscription model contains:
//	* Id: an instances unique database identifier
//	* Email: the email address of the subscriber
//	* Subscribed: specifies whether the contact is subscribed or has unsubscribed
//	* CreatedDate: when the subscription was created
//	* LastUpdated: when the subscription was last modified
type Subscription struct {
	Id				int64		`json:"id,omitempty"`
	Email			string		`json:"email,omitempty"`
	Subscribed		bool		`json:"subscribed,omitempty"`
	CreatedDate		time.Time	`json:"createdDate,omitempty"`
	LastUpdated		time.Time	`json:"lastUpdated,omitempty"`
}

// Creates a Subscription record in the database. An error is returned if the insert fails.
func (subscription *Subscription) Create(db *sql.DB) error {
	currentTime := time.Now().Truncate(time.Millisecond)
	subscription.CreatedDate = currentTime
	subscription.LastUpdated = currentTime

	_, insertionError := db.Exec(
		`INSERT INTO subscriptions (email, created_date, last_updated) VALUES ($1, $2, $3)`,
		subscription.Email, subscription.CreatedDate, subscription.LastUpdated,
	)

	if insertionError != nil {
		logrus.WithFields(logrus.Fields{
			"subscription": subscription,
			"error": insertionError,
		}).Error("failed to persist subscription")
		return errors.New("failed to create subscription")
	}

	return nil
}

// Converts a sql.Rows result set into a Subscription instance
// If scan fails an error is returned.
func scanSubscription(result *sql.Rows) (Subscription, error) {
	subscription := Subscription{}

	scanError := result.Scan(
		&subscription.Id,
		&subscription.Email,
		&subscription.Subscribed,
		&subscription.CreatedDate,
		&subscription.LastUpdated,
	)

	return subscription, scanError
}

// Retrieves a Subscription from the database
// An error is returned if retrieval fails or if scanning the model fails.
func GetSubscriptionByEmail(db *sql.DB, email string) (Subscription, error) {

	var subscription Subscription

	result, queryError := db.Query(
		`SELECT id, email, subscribed, created_date, last_updated FROM subscriptions WHERE email = $1`, email)
	if queryError != nil {
		errorMessage := "failed to query database for subscription"
		logrus.WithFields(logrus.Fields{
			"email": email,
			"error": queryError.Error(),
		}).Error(errorMessage)
		return subscription, errors.New(errorMessage)
	}

	defer result.Close()
	for result.Next() {
		return scanSubscription(result)
	}

	return subscription, nil
}

// Updates an existing Subscription record, specifically the subscribed value, in the database.
func (subscription *Subscription) Update(db *sql.DB) error {
	subscription.LastUpdated = time.Now().Truncate(time.Millisecond)

	_, updateError := db.Exec(
		`UPDATE subscriptions SET subscribed = $1, last_updated = $2 WHERE email = $3`,
		subscription.Subscribed, subscription.LastUpdated, subscription.Email,
	)

	return updateError
}