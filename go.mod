module gitlab.com/thebeardedone/customer-communication-service

go 1.13

require (
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/emersion/go-smtp v0.14.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.2.2
	gitlab.com/thebeardedone/smtp-test-server v0.0.0-20201016211045-b77daef3ffc4
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
)
