package config

// Application model containing:
//	* Port:	port on which the application will listen on
//	* MaxNameLength: maximum length of a contact request name
//	* MaxSubjectLength: maximum length of a contact request subject
//	* MaxMessageLength: maximum length of a contact request message
type Application struct {
	Port             int `json:"port"`
	MaxNameLength    int `json:"maxNameLength"`
	MaxSubjectLength int `json:"maxSubjectLength"`
	MaxMessageLength int `json:"maxMessageLength"`
}