package config

// SMTPCredentials contains the username and password to be used when attempting to authenticate with an SMTP server.
type SMTPCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// SMTP configuration model containing:
//	* ContactAddress: the sender and receiver of emails
//	* Credentials: credentials used for authenticating with the SMTP server
//	* Gateway: SMTP server hostname or ip address
//	* Port: the port that the SMTP server is listening on
type SMTP struct {
	ContactAddress	string			`json:"contactAddress"`
	Credentials    	SMTPCredentials `json:"credentials"`
	Gateway        	string          `json:"gateway"`
	Port           	int             `json:"port"`
}