package config

import (
	"database/sql"
	"encoding/json"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
)

// serverConfiguration is a model that defines:
//	* Application: the application configuration model
//	* Database: the database configuration model
//	* SMTP:	the SMTP configuration model
type serverConfiguration struct {
	Application		Application `json:"application"`
	Database		Database    `json:"database"`
	SMTP			SMTP        `json:"smtp"`
}

// Context holding references to:
//	* Config: a reference to the application configuration
//	* Db: a reference to the database connection
//	* Smtp: a reference to the SMTP client configuration
type Context struct {
	Config     Application
	Db         *sql.DB
	Smtp       SMTP
}

// Reads the configuration file in the specified file location and attempts unmarshal the content into a
// serverConfiguration object
func ReadConfigurationFile(fileLocation string) serverConfiguration {
	serverConfig := serverConfiguration{}

	logrus.WithFields(logrus.Fields{
		"fileLocation": fileLocation,
	}).Info("reading configuration file")

	configFile, err := os.Open(fileLocation)
	byteValue, _ := ioutil.ReadAll(configFile)

	if err != nil {
		logrus.WithField("file", fileLocation).Panic("failed to find configuration file")
	}

	unmarshalError := json.Unmarshal(byteValue, &serverConfig)
	if unmarshalError != nil {
		logrus.Error(unmarshalError.Error())
		logrus.Panic("failed to unmarshal server configuration")
	}

	return serverConfig
}

// Default Context constructor
//
// Accepts the location to the configuration file. Constructor will set the log format to JSON, attempt to read the
// configuration file and unmarshal it into a serverConfiguration object. A database connection will be opened and
// a context will be returned.
func NewContext(fileLocation string) Context {

	logrus.SetFormatter(&logrus.JSONFormatter{})

	serverConfig := ReadConfigurationFile(fileLocation)

	serverContext := Context{
		Config: serverConfig.Application,
		Db:     serverConfig.Database.Connect(),
		Smtp:   serverConfig.SMTP,
	}

	return serverContext
}
