package config

import (
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"strconv"
)

// Database configuration model used to build a connection. The model requires the following details:
//  * Name: database name
//	* User: username of database user
//	* Password: password of database user
//	* Host: database hostname or ip address
//	* Port: port the database is listening on
//	* ConnectTimeout: maximum wait for connection, in seconds. Zero or not specified means wait indefinitely.
//	* SSLMode: specifies whether the database communication will be performed with SSL/TLS
// 		* disable - No SSL
//		* require - Always SSL (skip verification)
//		* verify-ca - Always SSL (verify that the certificate presented by the server was signed by a trusted CA)
//		* verify-full - Always SSL (verify that the certification presented by the server was signed by a trusted CA and
//		the server host name matches the one in the certificate)
//	* SSLCertificate: certificate file location
//	* SSLKey: key file location
//	* SSLRootCertificate: root certificate file location
type Database struct {
	Name               string `json:"name"`
	User               string `json:"user"`
	Password           string `json:"password"`
	Host               string `json:"host"`
	Port               int    `json:"port"`
	ConnectTimeout     int    `json:"connectTimeout"`
	SSLMode            string `json:"sslMode"`
	SSLCertificate     string `json:"sslCertificate"`
	SSLKey             string `json:"sslKey"`
	SSLRootCertificate string `json:"sslRootCertificate"`
}

// Connect configures a connection string and attempts to create a database connection. If an error occurs while
// opening the connection, panic will be called and the reason will be logged.
func (database *Database) Connect() *sql.DB {

	connectionString := " dbname=" + database.Name
	connectionString += " user=" + database.User
	connectionString += " password=" + database.Password
	connectionString += " host=" + database.Host
	connectionString += " port=" + strconv.Itoa(database.Port)
	connectionString += " connect_timeout=" + strconv.Itoa(database.ConnectTimeout)
	connectionString += " sslmode=" + database.SSLMode

	if database.SSLMode != "disable" {
		connectionString += " sslcert=" + database.SSLCertificate
		connectionString += " sslkey=" + database.SSLKey
		connectionString += " sslrootcert=" + database.SSLRootCertificate
	}

	db, connectionError := sql.Open("postgres", connectionString)

	if connectionError != nil {
		logrus.WithFields(logrus.Fields{
			"connectionString": connectionString,
			"error":            connectionError.Error(),
		}).Panic("failed to connect to database")
	}

	return db
}
