package api

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/customer-communication-service/config"
	"gitlab.com/thebeardedone/customer-communication-service/utils"
	"net/http"
)

// Enters the submitted email address in the database if it does not exist or updates the subscription value to true if
// the email address already exists.
//
// An empty response is returned with status code 200 if the record is successfully inserted into the database.
// An error will be returned if:
//  * 400 - subscription request is malformed
//  * 400 - email address fails validation
//  * 500 - record retrieval fails
//	* 500 - insert of the subscription record fails
//	* 500 - update of the subscription record fails
//
// Failure of sending an email will log a warning but will not return an error.
func Subscribe(context *config.Context, route string) http.HandlerFunc {
	return func(writer http.ResponseWriter, reader *http.Request) {

		correlationId := reader.Header.Get("X-Correlation-ID")

		subscription := utils.Subscription{}
		decodeError := json.NewDecoder(reader.Body).Decode(&subscription)
		if decodeError != nil {
			errorMessage := "failed to parse subscription"
			logrus.WithFields(logrus.Fields{
				"correlationId": correlationId,
				"error": decodeError,
				"route": route,
				"subscription": reader.Body,
			}).Error(errorMessage)
			utils.OnError(writer, http.StatusBadRequest, errorMessage)
			return
		}

		emailValidationError := utils.ValidateEmailAddress(subscription.Email)
		if emailValidationError != nil {
			logrus.WithFields(logrus.Fields{
				"correlationId": correlationId,
				"error": emailValidationError,
				"route": route,
				"subscription": subscription.Email,
			}).Error("failed to validate email address")
			utils.OnError(writer, http.StatusBadRequest, emailValidationError.Error())
			return
		}

		retrievedSubscription, subscriptionRetrievalError := utils.GetSubscriptionByEmail(context.Db, subscription.Email)
		if subscriptionRetrievalError != nil {
			errorMessage := "failed to validate whether subscription exists"
			logrus.WithFields(logrus.Fields{
				"correlationId": correlationId,
				"email": subscription.Email,
				"error": subscriptionRetrievalError,
				"route": route,
			}).Error(errorMessage)
			utils.OnError(writer, http.StatusInternalServerError, errorMessage)
			return
		}

		if retrievedSubscription.Id == 0 {
			creationError := subscription.Create(context.Db)
			if creationError != nil {
				logrus.WithFields(logrus.Fields{
					"correlationId": correlationId,
					"error": creationError,
					"route": route,
					"subscription": subscription,
				}).Error("failed to create subscription")
				utils.OnError(writer, http.StatusInternalServerError, creationError.Error())
				return
			}
		} else {
			retrievedSubscription.Subscribed = true
			updateError := retrievedSubscription.Update(context.Db)
			if updateError != nil {
				errorMessage := "failed to update subscription"
				logrus.WithFields(logrus.Fields{
					"correlationId": correlationId,
					"error": updateError,
					"route": route,
					"subscription": retrievedSubscription,
				}).Error(errorMessage)
				utils.OnError(writer, http.StatusInternalServerError, errorMessage)
				return
			}
		}

		logrus.WithFields(logrus.Fields{
			"correlationId": correlationId,
			"route": route,
		}).Info("successfully subscribed an email address")
		utils.OnSuccess(writer, nil)
	}
}