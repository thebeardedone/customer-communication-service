package api

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/thebeardedone/customer-communication-service/test"
	"gitlab.com/thebeardedone/customer-communication-service/utils"
	"net/http"
	"testing"
)

func TestIntegrationSubscribe(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	subscription := utils.Subscription{
		Email: "test@test.com",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Subscribe), subscription)
	assert.Equal(tester, http.StatusOK, response.StatusCode)
}

func TestIntegrationSubscribeDuplicate(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	subscription := utils.Subscription{
		Email: "test@test.com",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Subscribe), subscription)
	assert.Equal(tester, http.StatusOK, response.StatusCode)

	response = client.Post(testContext.GetFullURL(test.Subscribe), subscription)
	assert.Equal(tester, http.StatusOK, response.StatusCode)
}

func TestIntegrationSubscribeInvalidEmailAddress(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	invalidEmails := []string {
		"test",
		"test@test",
		"test.com",
	}

	testContext := getTestContext()
	client := test.NewHttpClient(tester)

	for _, email := range invalidEmails {
		subscription := utils.Subscription{
			Email: email,
		}
		response := client.Post(testContext.GetFullURL(test.Subscribe), subscription)
		assert.Equal(tester, http.StatusBadRequest, response.StatusCode)
	}
}