package api

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/thebeardedone/customer-communication-service/test"
	"gitlab.com/thebeardedone/customer-communication-service/utils"
	"net/http"
	"testing"
)

// Sending a valid contact request will create a record in the database and send an email to the configured email
// address.
func TestIntegrationContact(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "test@test.com",
		Subject: "Test message",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusOK, response.StatusCode,
		"creating a valid contact request should return http status code 200 ok")

	email, emailRetrievalError := testContext.SMTPBackEnd.GetEmail()
	assert.Nil(tester, emailRetrievalError, "an email should be available in the backend")
	assert.Equal(tester, "New contact request! " + contactRequest.Subject, email.Subject,
		"the subject should have 'New contact request!' prepended to the subject")
	expectedBody := "From: " + contactRequest.Name + "<" + contactRequest.Email + ">\r\n" +
		"Message:\r\n" + contactRequest.Message
	assert.Equal(tester, expectedBody, email.TextBody)
}

// Attempting to create a contact request with an empty name will return an error.
func TestIntegrationContactNameEmpty(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "",
		Email:   "test@test.com",
		Subject: "Test message",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "name is mandatory", parseErrorMessageFromResponse(tester, response),
		"an error message indicated that the name is mandatory is returned")
}

// Attempting to create a contact request with a name that is too long will return an error.
func TestIntegrationContactNameTooLong(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "",
		Email:   "test@test.com",
		Subject: "Test message",
		Message: "This is a test message",
	}

	for i := 1; i <= 51; i++ {
		contactRequest.Name += "a"
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "name can have at most 50 characters", parseErrorMessageFromResponse(tester, response),
		"an error indicating that the name is too long is returned")
}

// Attempting to create a contact request with a new line character in the name will return an error.
func TestIntegrationContactNameNewLineCharacter(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test\n",
		Email:   "test@test.com",
		Subject: "Test message",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "name contains an invalid character", parseErrorMessageFromResponse(tester, response),
		"an error indicating that the name contains an invalid character is returned")

	contactRequest.Name = "test\r"
	response = client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "name contains an invalid character", parseErrorMessageFromResponse(tester, response),
		"an error indicating that the name contains an invalid character is returned")
}

// Attempting to create a contact request with an empty email will return an error.
func TestIntegrationContactEmailEmpty(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "",
		Subject: "Test message",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "email address is mandatory", parseErrorMessageFromResponse(tester, response),
		"an error message indicated that the email address is mandatory is returned")
}

// Attempting to create a contact request with an invalid email address will return an error.
func TestIntegrationContactEmailInvalid(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	invalidEmails := []string {
		"test",
		"test@test",
		"test.com",
	}

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Subject: "Test message",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)

	for _, email := range invalidEmails {
		contactRequest.Email = email
		response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
		assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
			"creating an invalid contact request should return http status code 400 bad request")
		assert.Equal(tester, "invalid email address", parseErrorMessageFromResponse(tester, response),
			"an error message indicated that the email address is invalid is returned")
	}
}

// Attempting to create a contact request with an empty subject will return an error.
func TestIntegrationContactSubjectEmpty(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "test@test.com",
		Subject: "",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "subject is mandatory", parseErrorMessageFromResponse(tester, response),
		"an error message indicated that the subject is mandatory is returned")
}

// Attempting to create a contact request with an empty subject will return an error.
func TestIntegrationContactSubjectTooLong(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "test@test.com",
		Subject: "",
		Message: "This is a test message",
	}

	for i := 1; i <= 41; i++ {
		contactRequest.Subject += "a"
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "subject can have at most 40 characters", parseErrorMessageFromResponse(tester, response),
		"an error message indicated that the subject is too long is returned")
}

// Attempting to create a contact request with a new line character in the subject will return an error.
func TestIntegrationContactSubjectNewLineCharacter(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "test@test.com",
		Subject: "Test message\n",
		Message: "This is a test message",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "subject contains an invalid character", parseErrorMessageFromResponse(tester, response),
		"an error indicating that the subject contains an invalid character is returned")

	contactRequest.Subject = "Test message\r"
	response = client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "subject contains an invalid character", parseErrorMessageFromResponse(tester, response),
		"an error indicating that the subject contains an invalid character is returned")
}

// Attempting to create a contact request with an empty message will return an error.
func TestIntegrationContactMessageEmpty(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "test@test.com",
		Subject: "Test message",
		Message: "",
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "message is mandatory", parseErrorMessageFromResponse(tester, response),
		"an error message indicated that the message is mandatory is returned")
}

// Attempting to create a contact request with a message that is too long will return an error.
func TestIntegrationContactMessageTooLong(tester *testing.T) {
	setUpTest(tester)
	defer tearDownTest(tester)

	testContext := getTestContext()
	contactRequest := utils.ContactRequest{
		Name:    "test",
		Email:   "test@test.com",
		Subject: "Test message",
		Message: "",
	}

	for i := 1; i <= 501; i++ {
		contactRequest.Message += "a"
	}

	client := test.NewHttpClient(tester)
	response := client.Post(testContext.GetFullURL(test.Contact), contactRequest)
	assert.Equal(tester, http.StatusBadRequest, response.StatusCode,
		"creating an invalid contact request should return http status code 400 bad request")
	assert.Equal(tester, "message can have at most 500 characters", parseErrorMessageFromResponse(tester, response),
		"an error message indicated that the message is too long is returned")
}