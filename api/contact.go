package api

import (
	"encoding/json"
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/customer-communication-service/config"
	"gitlab.com/thebeardedone/customer-communication-service/utils"
	"net/http"
	"strconv"
	"strings"
)

// Creates a contact request and forwards it to the configured contact email address.
//
// An empty response is returned with status code 200 if the message is successfully inserted into the database.
// An error will be returned if:
//  * 400 - contact request is malformed
//  * 400 - contact request fails validation
//	* 500 - insert of the contact request record fails
//
// Failure of sending an email will log a warning but will not return an error.
func Contact(context *config.Context, route string) http.HandlerFunc {
	return func(writer http.ResponseWriter, reader *http.Request) {

		correlationId := reader.Header.Get("X-Correlation-ID")

		contactRequest := utils.ContactRequest{}
		decodeError := json.NewDecoder(reader.Body).Decode(&contactRequest)
		if decodeError != nil {
			errorMessage := "failed to parse contact request"
			logrus.WithFields(logrus.Fields{
				"contactRequest": reader.Body,
				"correlationId": correlationId,
				"error": decodeError,
				"route": route,
			}).Error(errorMessage)
			utils.OnError(writer, http.StatusBadRequest, errorMessage)
			return
		}

		validationError := contactRequest.Validate(context)
		if validationError != nil {
			logrus.WithFields(logrus.Fields{
				"contactRequest": contactRequest,
				"correlationId": correlationId,
				"error": validationError,
				"route": route,
			}).Error("failed to validate contact request")
			utils.OnError(writer, http.StatusBadRequest, validationError.Error())
			return
		}

		creationError := contactRequest.Create(context.Db)
		if creationError != nil {
			logrus.WithFields(logrus.Fields{
				"contactRequest": contactRequest,
				"correlationId": correlationId,
				"error": creationError,
				"route": route,
			}).Error("failed to create contact request")
			utils.OnError(writer, http.StatusInternalServerError, creationError.Error())
			return
		}

		authentication := sasl.NewPlainClient(
			"", context.Smtp.Credentials.Username, context.Smtp.Credentials.Password)
		msg := strings.NewReader(
			"From: \"Customer Communication Service\"<" + context.Smtp.ContactAddress + ">\n" +
			"Subject: New contact request! " + contactRequest.Subject + "\n" +
			"MIME-version: 1.0;\nContent-Type: text/plain; charset=\"UTF-8\";\n\n" +
			"From: " + contactRequest.Name + "<" + contactRequest.Email + ">\n" +
			"Message:\n" + contactRequest.Message)

		gatewayUrl := context.Smtp.Gateway + ":" + strconv.Itoa(context.Smtp.Port)
		sendError := smtp.SendMail(
			gatewayUrl, authentication, "username", []string{context.Smtp.ContactAddress}, msg)
		if sendError != nil {
			logrus.WithFields(logrus.Fields{
				"contactAddress": context.Smtp.ContactAddress,
				"contactRequest": contactRequest,
				"correlationId": correlationId,
				"error": sendError,
				"gateway": gatewayUrl,
				"route": route,
			}).Warn("failed to forward contact request to gateway")
		}

		logrus.WithFields(logrus.Fields{
			"correlationId": correlationId,
			"route": route,
		}).Info("successfully processed contact request")
		utils.OnSuccess(writer, nil)
	}
}
