package api

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/thebeardedone/customer-communication-service/test"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
)

var testContext test.Context

// When running the test suite, setUpSuite is called prior and tearDownSuite after running the tests.
func TestMain(m *testing.M) {
	integrationTestContext := setUpSuite()
	retCode := m.Run()
	teatDownSuite(integrationTestContext)
	os.Exit(retCode)
}

// The current test case name is printed prior to running the test
func setUpTest(tester *testing.T) {
	logrus.Debug("Preparing for " + tester.Name())
}

// Database tables are truncated and all SMTP emails are removed after each test
func tearDownTest(tester *testing.T) {
	logrus.Debug("Cleaning up for " + tester.Name())
	test.TruncateTable(testContext.Db, "contact_requests")
	test.TruncateTable(testContext.Db, "subscriptions")
	testContext.SMTPBackEnd.ClearEmails()
}

// Sets up the test suite by initializing the logger, reading the test configuration file and starting a test SMTP
// server
func setUpSuite() test.Context {

	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})

	testContext = test.NewContext("../test-config.json")

	go testContext.SMTPBackEnd.Start()

	return testContext
}

// Closes all open database connections and stops the test SMTP server
func teatDownSuite(testContext test.Context) {
	testContext.Db.Close()
	testContext.SMTPBackEnd.Stop()
}

// Returns the current test context
func getTestContext() *test.Context {
	return &testContext
}

// Helper function used to read the body of a response and returns it as a byte array
// The test will fail if the parser returns an error.
func readBody(tester *testing.T, body io.ReadCloser) []byte {
	defer body.Close()
	responseBody, bodyReadError := ioutil.ReadAll(body)
	assert.Nil(tester, bodyReadError, "body parser should not return an error")
	return responseBody
}

// Helper function used to unmarshal the body of a response into an error message which is returned
// The test will fail if the parser returns an error or if unmarshalling fails.
func parseErrorMessageFromResponse(tester *testing.T, response *http.Response) string {
	var errorResponse map[string]string
	unmarshalError := json.Unmarshal(readBody(tester, response.Body), &errorResponse)
	assert.Nil(tester, unmarshalError, "failed to unmarshal error response")
	return errorResponse["error"]
}

