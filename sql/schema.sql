CREATE TABLE IF NOT EXISTS contact_requests (
    id              BIGSERIAL PRIMARY KEY,
    name            VARCHAR NOT NULL,
    email           VARCHAR NOT NULL,
    subject         VARCHAR NOT NULL,
    message         VARCHAR NOT NULL,
    unread          BOOLEAN DEFAULT TRUE,
    created_date    TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated    TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE IF NOT EXISTS subscriptions (
    id              BIGSERIAL PRIMARY KEY,
    email           VARCHAR UNIQUE NOT NULL,
    subscribed      BOOLEAN DEFAULT TRUE,
    created_date    TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated    TIMESTAMP WITH TIME ZONE NOT NULL
);