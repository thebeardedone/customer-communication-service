package test

import (
	"database/sql"
	"github.com/sirupsen/logrus"
)

// Truncates the specified database table
func TruncateTable(db *sql.DB, table string) {
	logrus.Debug("truncating all data in table " + table)
	_, deletionError := db.Exec(`TRUNCATE ` + table + ` RESTART IDENTITY CASCADE`)

	if deletionError != nil {
		logrus.Error(deletionError.Error())
	} else {
		logrus.Debug("successfully truncated all data in table " + table)
	}
}