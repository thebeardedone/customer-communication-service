package test

import (
	"bytes"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"testing"
	"time"
)

// Test HttpClient
type HttpClient struct {
	Client *http.Client
	Tester *testing.T
}

// Default constructor for HttpClient
//
// A reference to the provided testing.T is stored for the use in operation. The default time limit for requests is set
// to 1 second.
func NewHttpClient(tester *testing.T) *HttpClient {
	return &HttpClient{
		Client: &http.Client{
			Timeout: time.Second * 1,
		},
		Tester: tester,
	}
}

// Performs an http POST request on the specified URL with the specified body data
func (httpClient *HttpClient) Post(url string, body interface{}) *http.Response {
	return httpClient.performRequest(http.MethodPost, url, body)
}

// Creates a request for the specified HTTP method using the URL and data provided, performs the request and if an error
// occurs during the generation of a request or execution of the request, FailNow() will be called on testing.T.
func (httpClient *HttpClient) performRequest(method string, url string, body interface{}) *http.Response {

	var request *http.Request
	var requestGenerationError error

	if body == nil {
		request, requestGenerationError = http.NewRequest(method, url, nil)
	} else {
		request, requestGenerationError = http.NewRequest(method, url, Marshal(httpClient.Tester, body))
	}

	if requestGenerationError != nil {
		logrus.WithField("error", requestGenerationError.Error()).Error("failed to create request")
		httpClient.Tester.FailNow()
	}

	request.Header.Set("Content-Type", "application/json")
	response, postError := httpClient.Client.Do(request)

	if postError != nil {
		logrus.WithField("error", postError.Error()).Error("failed to perform request")
		httpClient.Tester.FailNow()
	}

	return response
}

// Marshal accepts testing.T and an object to convert to JSON. If marshalling fails FailNow() will be called.
func Marshal(tester *testing.T, object interface{}) *bytes.Buffer {
	objectJson, marshalError := json.Marshal(object)

	if marshalError != nil {
		logrus.WithField("error", marshalError.Error()).Error("failed to marshal object")
		tester.FailNow()
	}

	return bytes.NewBufferString(string(objectJson))
}