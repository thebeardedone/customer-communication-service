package test

import (
	"database/sql"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/customer-communication-service/config"
	smtpConfig "gitlab.com/thebeardedone/smtp-test-server/config"
	smtpUtils "gitlab.com/thebeardedone/smtp-test-server/utils"
	"io/ioutil"
	"os"
)

// serverConfiguration is a model that defines:
//	* BaseURL: base URL of the api server
//	* Database: the database configuration model
//	* SMTP:	the SMTP configuration model
type serverConfiguration struct {
	BaseURL	 		string			 		`json:"baseURL"`
	Database 		config.Database 		`json:"database"`
	SMTP     		config.SMTP     		`json:"smtp"`
}

// Test Context holding references to:
//	* BaseURL: base URL of the api server
//	* Config: a reference to the application configuration
//	* Db: a reference to the database connection
//	* SMTPBackEnd: a reference to the test SMTP server
type Context struct {
	BaseURL					string
	Config					serverConfiguration
	Db						*sql.DB
	SMTPBackEnd 			*smtpUtils.SMTPBackEnd
}

// Reads the test configuration file in the specified file location and attempts unmarshal the content into a
// serverConfiguration object
func readConfigurationFile(fileLocation string) serverConfiguration {
	serverConfig := serverConfiguration{}

	configFile, err := os.Open(fileLocation)
	byteValue, _ := ioutil.ReadAll(configFile)

	if err != nil {
		logrus.Error(err.Error())
		logrus.Panic("failed to find configuration file")
	}

	unmarshalError := json.Unmarshal(byteValue, &serverConfig)

	if unmarshalError != nil {
		logrus.Error(unmarshalError.Error())
		logrus.Panic("failed to unmarshal server configuration")
	}

	return serverConfig
}

// Default Test Context constructor
//
// Accepts the location to the configuration file. Constructor will set the log format to JSON, attempt to read the
// configuration file and unmarshal it into a serverConfiguration object. A database connection will be opened, an SMTP
// server is started in a separate channel and a test context will be returned.
func NewContext(fileLocation string) Context {

	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})

	testConfig := readConfigurationFile(fileLocation)

	smtpServerConfig := smtpConfig.SMTP{
		Credentials:     smtpConfig.SMTPCredentials{
			Username: testConfig.SMTP.Credentials.Username,
			Password: testConfig.SMTP.Credentials.Password,
		},
		Domain: "test.com",
		GetEmailTimeout: 1,
		Port: testConfig.SMTP.Port,
	}

	testContext := Context{
		BaseURL:    testConfig.BaseURL,
		Config:     testConfig,
		Db:         testConfig.Database.Connect(),
		SMTPBackEnd: smtpUtils.NewSMTPBackEnd(smtpServerConfig),
	}

	return testContext
}

// Definition of endpoints
const (
	Contact       	string = "/contact"
	Subscribe		string = "/subscribe"
)

// Automatically generates the full URL (baseURL + endpoint) dependent on the route that is passed. Panic is called if
// the endpoint does not exist.
func (ctx *Context) GetFullURL(route string) string {
	url := ctx.BaseURL

	switch route {
	case Contact:
		url += Contact
		break
	case Subscribe:
		url += Subscribe
		break
	default:
		logrus.WithFields(logrus.Fields{
			"route": route,
		}).Panic("route not defined")
	}

	return url
}