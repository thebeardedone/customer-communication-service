package main

import (
	"context"
	"flag"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/thebeardedone/customer-communication-service/api"
	"gitlab.com/thebeardedone/customer-communication-service/config"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

func main() {

	configFile := flag.String("configFile", "./config.json", "path to json configuration file")
	flag.Parse()

	serverContext := config.NewContext(*configFile)

	router := mux.NewRouter()
	router.HandleFunc("/api/contact", api.Contact(&serverContext, "contact")).Methods(http.MethodPost)
	router.HandleFunc("/api/subscribe", api.Subscribe(&serverContext, "subscribe")).Methods(http.MethodPost)

	server := Start(serverContext.Config.Port, router)
	<-WaitForStopSignal()
	Stop(server)
}

func Start(port int, router *mux.Router) http.Server {

	server := http.Server{
		Addr:         ":" + strconv.Itoa(port),
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	logrus.WithFields(logrus.Fields{
		"version": getVersion(),
		"commit": GitCommit,
		"branch": GitBranch,
		"state": GitState,
		"built": BuildDate,
	}).Info("starting server")
	go func() {
		logrus.Panic(server.ListenAndServe())
	}()
	logrus.WithFields(logrus.Fields{
		"port": port,
	}).Info("server is running")

	return server
}

func WaitForStopSignal() chan os.Signal {
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, syscall.SIGQUIT)
	return stop
}

func Stop(server http.Server) {
	logrus.Info("shutting down server")

	if err := server.Shutdown(context.Background()); err != nil {
		logrus.Error("failed to gracefully shutdown server")
	}

	logrus.Info("gracefully shutdown server")
}
